const OAuth2Server = require('oauth2-server');
const {
	OAuthService,
} = require('./services');


const oauth = new OAuth2Server({
	model: OAuthService,
	accessTokenLifetime: 60 * 60,
	allowBearerTokensInQueryString: true,
	requireClientAuthentication: {
		password: false,
		refresh_token: false,
	},
});

module.exports = oauth;

