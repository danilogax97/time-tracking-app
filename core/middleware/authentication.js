const UnauthorizedError = require('../exceptions/unauthorized');

const oauth = require('../authentication');

const {
    Request,
    Response,
} = require('oauth2-server');

function authenticatedUser(req, res, next) {
    if (res.locals.token) {
        return next(null, req, res, next);
    }

    return next(new UnauthorizedError());
}

function authenticateUser(req, res, next) {

    const request = new Request(req);
    const response = new Response(res);

    oauth.authenticate(request, response)
        .then((token) => {
            res.locals.token = token;
            next(null)
        })
        .catch((err) => next(new UnauthorizedError(err)));

}

module.exports = {
    authenticateUser,
    authenticatedUser
};
