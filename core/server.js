const express = require('express');
const bodyParser = require('body-parser');
const expressDomain = require('express-domain-middleware');
const glob = require('glob');
const error = require('./middleware/error');
const formatter = require('./middleware/formatter');
const path = require('path');
const app = express();

// Error code
const NotFoundError = require('./exceptions/notfound');

app.set('trust proxy', true);
app.use(expressDomain);

app.use((req, res, next) => {
    const {
        origin,
    } = req.headers;

    if ((origin != null) && (origin.length > 0)) {
        res.setHeader('Access-Control-Allow-Origin', origin);
        res.header('Access-Control-Allow-Credentials', 'true');
        res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,PATCH,DELETE');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    }

    next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true,
}));

//Add routers
glob('**/*.js', {
    cwd: path.resolve(`${__dirname}/../controllers/`),
}, (err, files) => {
    files.forEach((file) => {
        const split = file.split('/');
        const controller = require(`${__dirname}/../controllers/${file}`); // eslint-disable-line
        if (typeof controller === 'function') {
            app.use(`/${split[0]}`, controller);
        }
    });

    app.use(async (req, res, next) => {
        if (!res.locals.data) {
            return next(new NotFoundError('Route not matched'));
        }
        return next(null, req, res, next);
    });

    app.use(error);

    app.use(formatter);
});


module.exports = app;
