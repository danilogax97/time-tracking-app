const BaseError = require('../../core/exceptions/index');
const NotFoundError = require('../../core/exceptions/notfound');
const express = require('express');
const {
	UserService,
	OAuthAccessTokenService,
	OAuthClientService,
    CompanyService,
} = require('../../core/services');
const {
	authenticateUser
} = require('../../core/middleware/authentication');
const {
	Request,
	Response
} = require('oauth2-server');
const oauth = require('../../core/authentication');

const User = express.Router();
// @TODO validation middleware
User.post('/sign-up', async (req, res, next) => {
	try {
		const {
			email,
			name,
			password,
			clientId
		} = req.body;

		//const client = await OAuthClientService.getClient(Buffer.from(req.headers.authorization.split(" ")[1], 'base64').toString());
		const client = await OAuthClientService.getClient(clientId);

		if (!client) {
			throw new NotFoundError('OAuth client not found');
        }

		const user = await UserService.getUserByEmail(email);

		if (user) {
			// @TODO solve this
			// Koju vrstu greske koristim u ovom slucaju?
			next(new BaseError("user already exists", "description"))
		}
		await UserService.create({
			email,
			fullName: name,
			passwordPlain: password,
            companyId: 'defaultCompanyId',
		});

		res.locals.data = {
			success: true
		};
		next(null);
	} catch (error) {
		next(error)
	}
});

//this is route for getting access token
User.post('/sign-in', async (req, res, next) => {
	try {

		const request = new Request(req);
		const response = new Response(res);

		//@TODO maybe filter this response
		res.locals.data = await oauth.token(request, response);

		next(null);
	} catch (error) {
		next(error);
	}
});

User.delete('/sign-out', authenticateUser, async (req, res, next) => {
	try {
		// Destroy token
		res.locals.data = await OAuthAccessTokenService.logout(res.locals.token.accessToken);
		return next(null);
	} catch (err) {
		return next(err);
	}
});


module.exports = User;
