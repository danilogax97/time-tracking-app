const express = require('express');
const moment = require('moment');
const Activity = express.Router();
const BadRequest = require('../../core/exceptions/badrequest');
const {
    TimeService,
    UserService
} = require('../../core/services');
const {
    authenticateUser
} = require('../../core/middleware/authentication');


Activity.get('/reports', authenticateUser, async (req, res, next) => {
    try {
        const reports = await TimeService.getTimesFromUserIdDescending(res.locals.token.user.id);

        //TODO maybe filter reports
        res.locals.data = {reports};
        next(null)
    } catch (error) {
        next(error);
    }
});

Activity.post('/add-activity', authenticateUser, async (req, res, next) => {
    try {
        const {
            startTime,
            endTime,
        } = req.body;

        const user = await UserService.getUserByEmail(res.locals.token.user.email);

        const time = await TimeService.create({
            startTime: startTime,
            endTime: endTime
        });
        await user.addTimes([time]);

        res.locals.data = {success: true};
        next(null)
    } catch (error) {
        next(error);
    }
});
Activity.post('/clock-in', authenticateUser, async (req, res, next) => {
    try {
        const startTime = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
        // @TODO fix this
        const user = await UserService.getUserByEmailWithStartTime(res.locals.token.user.email);

        if(user.times.length > 0){
            next(new BadRequest("User is already clocked in"));
            return;
        }

        const time = await TimeService.create({
            startTime: startTime,
        });

        await user.addTime(time);

        res.locals.data = {success: true, timerId: time.id};
        next(null)
    } catch (error) {
        next(error);
    }
});
Activity.post('/clock-out', authenticateUser, async (req, res, next) => {
    try {
        const {
            timerId,
        } = req.body;

        const time = await TimeService.getTimeFromId(timerId);
        const endTime = moment().format('YYYY-MM-DDTHH:mm:ss.SSSZ');

        if(!time){
            next(new BadRequest('There is no such activity'));
            return;
        }
        //should we check if the user already clockedOut?
        const newTime = await time.update(
                {
                    endTime: endTime
                },
                {
                    where:{
                        id:timerId
                    }
                });

        res.locals.data = {
            success: true,
            timerId: time.id,
            newTime
        };
        next(null)
    } catch (error) {
        next(error);
    }
});
Activity.post('/edit-activity/:timerId', authenticateUser, async (req, res, next) => {
    try {
        const {
            startTime,
            endTime,
        } = req.body;
        const { timerId } = req.params;

        const time = await TimeService.getTimeFromId(timerId);

        if(!time){
            next(new BadRequest("Can't find activity with that id"));
            return;
        }
        const updatedTime = await time.update(
            {
                startTime: startTime,
                endTime: endTime
            });
        res.locals.data = {
            success: true,
            updatedTime: updatedTime
        };
        next(null)
    } catch (error) {
        next(error);
    }
});
module.exports = Activity;

