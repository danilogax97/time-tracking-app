const sequelize = require('./core/database/postgresql');

const {
    User,
    Times,
    OAuthClient,
    Company,
} = require('./core/models');


sequelize.sync({force: true}).then(async () => {

    await OAuthClient.create({id: "application", clientSecret: "secret", grants: ["password", "refresh_token"]});
    await Company.create({id: "defaultCompanyId"});
    const user1 = await User.create({
        email: "mojemail@example.com",
        passwordPlain: "mojaprvasifra123",
        fullName: "marko",
    });
    const user2 = await User.create({
        email: "nekiemail@example.com",
        passwordPlain: "sifra123123",
        fullName: "ivan"
    });

    const time1 = await Times.create({startTime: '2019-09-03T12:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const startTime = await Times.create({startTime: '2019-09-03T12:12:46.011Z'});
    const time2 = await Times.create({startTime: '2019-09-03T11:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const time3 = await Times.create({startTime: '2019-09-03T10:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const time4 = await Times.create({startTime: '2019-09-03T23:12:46.011Z', endTime: '2019-09-03T23:59:46.011Z'});
    const time5 = await Times.create({startTime: '2019-09-03T12:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const time6 = await Times.create({startTime: '2019-09-03T12:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const time7 = await Times.create({startTime: '2019-09-03T12:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});
    const time8 = await Times.create({startTime: '2019-09-03T12:12:46.011Z', endTime: '2019-09-03T12:59:46.011Z'});


    await user1.setTimes([time1, time2, time3, time4, startTime]);
    await user2.setTimes([time5, time6, time7, time8]);

}).catch((error)=>{console.log("URGHGHGHGHGH", error)});
