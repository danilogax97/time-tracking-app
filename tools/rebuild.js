const models = require('../core/models');
const fs = require('fs');
const exec = require('child_process').execSync;
const redis = require('../core/database/redis');
const config = require('config');

const commands = config.get('tools.rebuild.scripts');

(async function rebuild() {
	try {
		const sqlLines = [];
		await models.sequelize.sync({
			force: true,
			logging: (message) => {
				let line = message.replace('Executing (default): ', '').trim();
				if (line.substr(-1) !== ';') {
					line += ';';
				}
				sqlLines.push(line);
			},
		});

		const sql = sqlLines.join('\n');
		// @TODO when doing rebuild, clean up migrations and start fresh
		fs.writeFile('./migrations/initial-tables.sql', sql, (error) => {
			if (error) {
				return console.log(error);
			}

			redis.flushdb();
			commands.forEach((command) => {
				console.info(`Running ${command}`);
				exec(command, { cwd: `${__dirname}/../`, shell: '/bin/bash', stdio: [0, 1, 2] });
			});
			console.log('completed');
			process.exit(0);
			return false;
		});
	} catch (err) {
		console.trace(err);
	}
}());
