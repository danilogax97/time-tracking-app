const config = require('config');
const app = require('./core/server');
// const sequelize = require('./core/database/postgresql');
//
// const {
//     OAuthClient,
//     User,
//     Times,
// } = require('./core/models');

const {
    port,
    host,
} = config.get('application');

// sequelize.sync({
//     force: true
// }).then(() => {
//     OAuthClient.create({id: "application", clientSecret: "secret", grants: ["password"]}).then(async () => {
//         console.log(await OAuthClient.findAll());
//         const user = await User.create({
//             email: "mojemail@example.com",
//             passwordPlain: "mojaprvasifra123",
//             fullName: "marko"
//         });
//         const time1 = await Times.create({startTime: Date.now(), endTime: Date.now()});
//         const time2 = await Times.create({startTime: Date.now(), endTime: Date.now()});
//         await user.setTimes([time1, time2]);
//         app.listen(port, host, () => console.log("Server started on " + host + ":" + port));
//     });
//
// });

//@NOTE run createdb time_tracking_test to make the database first
app.listen(port, host, () => console.log("Server started on " + host + ":" + port));

