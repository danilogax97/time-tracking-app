/**
 * Used for oauth2-server library, not  regular service
 * @fileOverview
 */
const {
	OAuthAccessToken,
	OAuthClient,
	User,
	Times,
} = require('../../core/models');
const NotFoundError = require('../../core/exceptions/notfound');
const NotImplementedError = require('../../core/exceptions/notimplemented');
const UnauthorizedError = require('../../core/exceptions/unauthorized');

class OAuth {
	static create(data) {
		return OAuthClient.create(data);
	}

	static getAccessToken(accessToken, callback) {
		OAuthAccessToken.findOne({
			where: {
				accessToken,
			},
			include: {
				model: User,
			},
		}).then((token) => {
			if (!token) {
				return callback(new NotFoundError('Token is invalid'));
			}

			return callback(null, {
				user: token.user,
				accessToken: token.accessToken,
				accessTokenExpiresAt: token.accessTokenExpiresAt,
				refreshToken: token.refreshToken,
				refreshTokenExpiresAt: token.refreshTokenExpires,
			});
		});
	}

	static getRefreshToken(refreshToken, callback) {
		OAuthAccessToken.findOne({
			where: {
				refreshToken,
			},
			include: [
				{
					model: OAuthClient,
				},
				{
					model: User,
				},
			],
		}).then((token) => {
			if (!token) {
				return callback(new NotFoundError('Refresh token is invalid'));
			}

			return callback(null, {
				client: token.oAuthClient,
				user: token.user,
				refreshToken: token.refreshToken,
				refreshTokenExpiresAt: token.refreshTokenExpires,
				accessToken: token.accessToken,
				accessTokenExpiresAt: token.refresh,
			});
		});
	}

	static revokeToken(accessToken, callback) {
		OAuthAccessToken.update({
			accessTokenExpiresAt: 0,
		}, {
			where: {
				accessToken: accessToken.accessToken,
			},
		}).then(() => {
			callback(null, {
				accessToken,
			});
		});
	}

	static getClient(clientId, clientSecret, callback) {
		OAuthClient.findOne({
			where: {
				id: clientId,
			},
		}).then((client) => {
			if (!client) {
				callback(new BaseError("non existent client"))
			}
			callback(null, client)
		})
	}

	static getUserFromClient(clientId, callback) {
		callback(null, {
			userId: '00000000-0000-0000-0000-000000000000',
		});
	}

	static getUser(email, password, callback) {

		User.findOne({
			where: {
				email: email
			},
			include: {
				model: Times,
				where: {
					endTime: null,
				},
				required: false,
				// as: 'startedTimers',
			}

		}).then((user) => {
			if (!user || !user.validPassword((password))) {
				callback(new UnauthorizedError())
			}
			callback(null, user)
		})

	}

	static saveToken(accessToken, client, user, callback) {

		OAuthAccessToken.upsert({
			accessToken: accessToken.accessToken,
			refreshToken: accessToken.refreshToken,
			accessTokenExpiresAt: accessToken.accessTokenExpiresAt,
			refreshTokenExpires: accessToken.refreshTokenExpiresAt,
			oAuthClientId: client.id,
			userId: user.id,
		}).then(() => {

			// OAuthAccessToken.findAll().then((res)=>console.log("tokens", res));
			callback(null, {
				accessToken,
				user,
				client,
			});
		});
	}

	// static validateScope(scope, callback) {
	// 	callback(null, true);
	// }

	static getAuthCode() {
		throw new NotImplementedError();
	}

	static saveAuthCode() {
		throw new NotImplementedError();
	}
}

module.exports = OAuth;
