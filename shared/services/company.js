const Service = require('./index');
const {Company} = require('../../core/models');

class CompanyService extends Service {

	static async getDefaultCompany() {
		const defaultCompany = await this.Model.findOne({});
		if(!defaultCompany){
			return this.Model.create({});
		}
		return defaultCompany
	}
}


CompanyService.Model = Company;

module.exports = CompanyService;
