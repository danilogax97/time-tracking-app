const Service = require('./index');
const {
    OAuthClient,
} = require('../../core/models');

/**
 * @extends Service
 * @namespace OAuthClientService
 * @class
 *
 * @property Model Hold main service model. Basically service is like extension of the model
 */
class OAuthClientService extends Service {
    static getClient(id) {
        return this.findOne({
            where: {
                id,
            },
        });
    }

    static getAllClients() {
        return this.findAll();
    }
}

OAuthClientService.Model = OAuthClient;

module.exports = OAuthClientService;
