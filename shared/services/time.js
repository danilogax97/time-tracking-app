const Service = require('./index');
const {Times} = require('../../core/models');
const Seqeulize = require('sequelize');
class TimeService extends Service {
	static getTimesFromUserIdDescending(userId) {
		return this.Model.findAll({
			where: {
				userId: userId,
				endTime: {
					[Seqeulize.Op.ne] : null,
				}
			},
			order: [
				['startTime', 'DESC']
			]
		})
	}
	static getAllTimesFromUser(id){
		return this.Model.findAll({

		})
	}
	static getTimeFromId(id) {
		return this.Model.findOne({
			where: {
				id: id,
			}
		})
	}
}


TimeService.Model = Times;

module.exports = TimeService;
