const Service = require('./index');
const {User} = require('../../core/models');
const {Times} = require('../../core/models');


class UserService extends Service {

    /**
     * @throws BadRequestError when there is no such account
     * @param email
     * @returns user
     */
    static getUserByEmail(email) {
        return this.findOne({
            where: {
                email: email
            },
        })
    }
    static getUserByEmailWithStartTime(email){
        return this.findOne({
            where: {
                email: email
            },
            include:{
                model: Times,
                where: {
                    endTime: null,
                },
                required: false,
            }
        })
    }
}

UserService.Model = User;

module.exports = UserService;
