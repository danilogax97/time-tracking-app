const Service = require('./index');
const {
	OAuthAccessToken,
} = require('../../core/models');

/**
 * @extends Service
 * @namespace OAuthAccessTokenService
 * @class
 *
 * @property Model Hold main service model. Basically service is like extension of the model
 */
class OAuthAccessTokenService extends Service {
	static logout(accessToken) {
		return this.Model.destroy({
			where: {
				accessToken,
			},
		});
	}
}

OAuthAccessTokenService.Model = OAuthAccessToken;

module.exports = OAuthAccessTokenService;
