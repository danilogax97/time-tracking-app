const Sequelize = require('sequelize');
const sequelize = require('../../core/database/postgresql');
const bcrypt =require('bcrypt');
const Times = require('./times');
const User = sequelize.define(
	'user',
	{
		id: {
			type: Sequelize.UUID,
			defaultValue: Sequelize.UUIDV4,
			primaryKey: true,
		},
		email: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				isEmail: true,
			},
			unique: 'Email address already in use!',
		},
		fullName: {
			type: Sequelize.STRING,
			allowNull: false,
			validate: {
				len: [1, 255],
			}
		},
		passwordPlain: {
			type: Sequelize.VIRTUAL,
			set: function (value) {
				this.setDataValue('password', bcrypt.hashSync(value, bcrypt.genSaltSync(10)));
			},
			validate: {
				len: [8, 255],
			},
		},
		password: {
			type: Sequelize.STRING,
			allowNull: true,
		},

	}
);

/**
 * Validate password
 * @memberOf User
 * @instance
 * @param {string} password User's password
 * @returns {Boolean}
 */
User.prototype.validPassword = function validPassword(password) {
	return bcrypt.compareSync(password, this.password);
};

User.associate = (db) => {
	const {
		Times,
        Company
	} = db;

	User.hasMany(Times);
	User.belongsTo(Company)
};
module.exports = User;
