const Sequelize = require('sequelize');
const sequelize = require('../../core/database/postgresql');

const AccessToken = sequelize.define(
	'oAuthAccessToken',
	{
		accessToken: Sequelize.STRING, // index this
		refreshToken: Sequelize.STRING, // index this
		accessTokenExpiresAt: Sequelize.DATE,
		refreshTokenExpires: Sequelize.DATE,
	},
	{
		indexes: [
			{
				fields: ['accessToken'],
			},
			{
				fields: ['refreshToken'],
			},
		],
	}
);


AccessToken.associate = (db) => {
	const {
		OAuthClient,
		User,
	} = db;

	AccessToken.belongsTo(OAuthClient);
	AccessToken.belongsTo(User);
};


module.exports = AccessToken;
