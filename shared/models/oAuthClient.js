const Sequelize = require('sequelize');
const sequelize = require('../../core/database/postgresql');

const OAuthClient = sequelize.define(
	'oAuthClient',
	{
		//TODO change id and clientsecret type to uuid
		id: {
			type: Sequelize.STRING,
			defaultValue: Sequelize.UUIDV4,
			primaryKey: true,
			unique: true,
		},
		grants: Sequelize.ARRAY(Sequelize.STRING),
		clientSecret: {
			type: Sequelize.STRING,
			defaultValue: Sequelize.UUIDV4,
		},
		clientType: {
			type: Sequelize.ENUM,
			values: ['public', 'confidential', 'web_application', 'native_application'],
			defaultValue: 'public',
		},
		redirectUri: Sequelize.STRING,
	}
);

module.exports = OAuthClient;
