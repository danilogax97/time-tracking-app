const Sequelize = require('sequelize');
const sequelize = require('../../core/database/postgresql');

const Times = sequelize.define(
    'times',
    {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
        },
        // userId: {
        //     type: Sequelize.UUID,
        //     foreignKey: true,
        // },
        startTime: {
            type: Sequelize.DATE,
            allowNull: false,
        },
        endTime: {
            type: Sequelize.DATE,
            defaultValue: null,
            allowNull: true,
        }
    }
);
Times.associate = (db) => {
    const {
        User,
    } = db;

    Times.belongsTo(User);
};

module.exports = Times;
