const Sequelize = require('sequelize');
const sequelize = require('../../core/database/postgresql');

const Company = sequelize.define(
	'company',
	{
		id: {
			type: Sequelize.STRING,
			primaryKey: true,
		},
		name:{
			type: Sequelize.STRING,
			defaultValue: "fatCatCoders",
			unique: true,
		}

	}
);


Company.associate = (db) => {
	const {
		User,
	} = db;
	Company.hasMany(User);
};
module.exports = Company;
